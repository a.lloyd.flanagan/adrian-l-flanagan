#!/usr/bin/python

# This code is the Mozilla html5check.py script, ported to Python 3 and
# substantially modified. The following license statement still applies. All
# modifications (to this file ONLY) are released under the same terms (i.e.
# you don't have to credit me, but you do have to credit Mozilla.)

# Copyright (c) 2007-2008 Mozilla Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

import http.client
import sys
import re
import urllib.parse
import gzip
import io


def main(argv):  # pylint: disable=too-many-branches

    filename = None
    force_xml = False
    force_html = False
    errors_only = False
    encoding = None
    service = 'http://html5.validator.nu/'

    for arg in argv:
        if '--help' == arg:
            print('-h : force text/html')
            print('-x : force application/xhtml+xml')
            print('-e : errors only (no info or warnings)')
            print('--encoding=foo : declare encoding foo')
            print('--service=url  : the address of the HTML5 validator')
            print('One file argument allowed. Leave out to read from stdin.')
            sys.exit(0)
        elif arg.startswith("--encoding="):
            encoding = arg[11:]
        elif arg.startswith("--service="):
            service = arg[10:]
        elif arg.startswith("--"):
            sys.stderr.write('Unknown argument %s.\n' % arg)
            sys.exit(2)
        elif arg.startswith("-"):
            for short_arg in arg[1:]:
                if 'x' == short_arg:
                    force_xml = True
                elif 'h' == short_arg:
                    force_html = True
                elif 'e' == short_arg:
                    errors_only = True
                else:
                    sys.stderr.write('Unknown argument %s.\n' % arg)
                    sys.exit(3)
        else:
            if filename:
                sys.stderr.write('Cannot have more than one input file.\n')
                sys.exit(1)
            filename = arg

        ext_pat = re.compile(r'^.*\.([A-Za-z]+)$')
        ext_dict = {
            "html" : "text/html",
            "htm" : "text/html",
            "xhtml" : "application/xhtml+xml",
            "xht" : "application/xhtml+xml",
            "xml" : "application/xml",
        }

        if force_xml and force_html:
            sys.stderr.write('Cannot force HTML and XHTML at the same time.\n')
            sys.exit(2)

        if force_xml:
            content_type = 'application/xhtml+xml'
        elif force_html:
            content_type = 'text/html'
        elif filename:
            match = ext_pat.match(filename)
            if match:
                ext = match.group(1).lower()
                if ext in ext_dict:
                    content_type = ext_dict[ext]
                else:
                    sys.stderr.write('Unable to guess Content-Type from '
                                     'file name. Please force the type.\n')
                    sys.exit(3)
            else:
                sys.stderr.write('Could not extract a filename extension. Please force the type.\n')
                sys.exit(6)
        else:
            sys.stderr.write('Need to force HTML or XHTML when reading from stdin.\n')
            sys.exit(4)

        with open(filename, 'r') as htmlin:
            html_string = htmlin.read()

        call_service(html_string, content_type,
                     encoding=encoding, service=service, errors_only=errors_only)


def call_service(html_string, content_type, encoding=None,
                 service='http://html5.validator.nu/', errors_only=False):

    if encoding:
        content_type = '%s; charset=%s' % (content_type, encoding)

    buf = io.BytesIO()
    gzipper = gzip.GzipFile(fileobj=buf, mode='wb')
    gzipper.write(html_string)
    gzipper.close()
    gzippeddata = buf.getvalue()
    buf.close()

    connection = None
    response = None
    status = 302
    redirect_count = 0

    url = service + '?out=text'

    if errors_only:
        url = url + '&level=error'

    while (status == 302 or status == 301 or status == 307) and redirect_count < 10:
        if redirect_count > 0:
            url = response.getheader('Location')
        parsed = urllib.parse.urlsplit(url)
        if parsed[0] != 'http':
            sys.stderr.write('URI scheme %s not supported.\n' % parsed[0])
            sys.exit(7)
        if redirect_count > 0:
            connection.close() # previous connection
            print('Redirecting to %s' % url)
            print('Please press enter to continue or type "stop" followed by enter to stop.')
            if input() != "":
                sys.exit(0)
        connection = http.client.HTTPConnection(parsed[1])
        connection.connect()
        connection.putrequest("POST", "%s?%s" % (parsed[2], parsed[3]), skip_accept_encoding=1)
        connection.putheader("Accept-Encoding", 'gzip')
        connection.putheader("Content-Type", content_type)
        connection.putheader("Content-Encoding", 'gzip')
        connection.putheader("Content-Length", len(gzippeddata))
        connection.endheaders()
        connection.send(gzippeddata)
        response = connection.getresponse()
        status = response.status
        redirect_count += 1

    if status != 200:
        sys.stderr.write('%s %s\n' % (status, response.reason))
        sys.exit(5)

    if response.getheader('Content-Encoding', 'identity').lower() == 'gzip':
        response = gzip.GzipFile(fileobj=io.StringIO(response.read()))

    sys.stdout.write(response.read().decode(sys.getdefaultencoding()))

    connection.close()


if __name__ == '__main__':
    main(sys.argv[1:])
